import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: 'manual-movements',
        loadChildren: () => import('./manual-movements/manual-movements.module').then(m => m.ManualMovementsModule)
      },
      { path: '', pathMatch: 'full', redirectTo: '/manual-movements' }
    ]),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
