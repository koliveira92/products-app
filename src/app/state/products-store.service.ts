import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Product } from '../manual-movements/models/product.model';
import { ProductCosif } from '../manual-movements/models/product-cosif.model';

@Injectable({
  providedIn: 'root'
})
export class ProductStoreService {

  private readonly isProductsLoadingSubject = new BehaviorSubject<boolean>(false);
  readonly isProductsLoading$ = this.isProductsLoadingSubject.asObservable();

  private readonly productsSubject = new BehaviorSubject<Array<Product>>(null);
  readonly products$ = this.productsSubject.asObservable();

  private readonly isProductCosifsLoadingSubject = new BehaviorSubject<boolean>(false);
  readonly isProductCosifsLoading$ = this.isProductCosifsLoadingSubject.asObservable();

  private readonly productCosifsSubject = new BehaviorSubject<Array<ProductCosif>>(null);
  readonly productCosifs$ = this.productCosifsSubject.asObservable();

  get isProductsLoading(): boolean {
    return this.isProductsLoadingSubject.getValue();
  }

  set isProductsLoading(value: boolean) {
    this.isProductsLoadingSubject.next(value);
  }

  get products(): Array<Product> {
    return this.productsSubject.getValue();
  }

  set products(value: Array<Product>) {
    this.productsSubject.next(value);
  }

  get isProductCosifsLoading(): boolean {
    return this.isProductCosifsLoadingSubject.getValue();
  }

  set isProductCosifsLoading(value: boolean) {
    this.isProductCosifsLoadingSubject.next(value);
  }

  get productCosifs(): Array<ProductCosif> {
    return this.productCosifsSubject.getValue();
  }

  set productCosifs(value: Array<ProductCosif>) {
    this.productCosifsSubject.next(value);
  }
}
