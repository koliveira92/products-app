import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ManualMovement } from '../manual-movements/models/manual-movements.model';

@Injectable({
  providedIn: 'root'
})
export class ManualMovementStoreService {

  private readonly isManualMovementsLoadingSubject = new BehaviorSubject<boolean>(false);
  readonly isManualMovementsLoading$ = this.isManualMovementsLoadingSubject.asObservable();

  private readonly manualMovementsSubject = new BehaviorSubject<Array<ManualMovement>>(null);
  readonly manualMovements$ = this.manualMovementsSubject.asObservable();

  get isManualMovementsLoading(): boolean {
    return this.isManualMovementsLoadingSubject.getValue();
  }

  set isManualMovementsLoading(value: boolean) {
    this.isManualMovementsLoadingSubject.next(value);
  }

  get manualMovements(): Array<ManualMovement> {
    return this.manualMovementsSubject.getValue();
  }

  set manualMovements(value: Array<ManualMovement>) {
    this.manualMovementsSubject.next(value);
  }
}
