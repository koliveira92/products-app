import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ManualMovementsPageComponent } from './presentation/pages/manual-movements-page/manual-movements-page.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: ManualMovementsPageComponent }
    ]),
  ],
  exports: [
    RouterModule
  ]
})
export class ManualMovementsRoutingModule { }
