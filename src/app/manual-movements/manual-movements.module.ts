import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';

import { ManualMovementsRoutingModule } from './manual-movements-routing.module';

import { ManualMovementsPageComponent } from './presentation/pages/manual-movements-page/manual-movements-page.component';
import { ManualMovementsFormComponent } from './presentation/components/manual-movements-form/manual-movements-form.component';
import { ManualMovementsListComponent } from './presentation/components/manual-movements-list/manual-movements-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatTableModule,
    ManualMovementsRoutingModule
  ],
  declarations: [
    ManualMovementsPageComponent,
    ManualMovementsFormComponent,
    ManualMovementsListComponent
  ],
  exports: [
    ManualMovementsPageComponent
  ]
})
export class ManualMovementsModule { }
