import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ManualMovement } from '../../models/manual-movements.model';
import { Product } from '../../models/product.model';
import { ProductCosif } from '../../models/product-cosif.model';
import { ManualMovementApiService } from '../../services/manual-movements-api.service';
import { ManualMovementStoreService } from '../../../state/manual-movements-store.service';
import { ProductApiService } from '../../services/products-api.service';
import { ProductStoreService } from '../../../state/products-store.service';

@Injectable({
  providedIn: 'root'
})
export class ManualMovementFacadeService {

  isManualMovementsLoading$: Observable<boolean>;

  manualMovements$: Observable<Array<ManualMovement>>;

  isProductsLoading$: Observable<boolean>;

  products$: Observable<Array<Product>>;

  isProductCosifsLoading$: Observable<boolean>;

  productCosifs$: Observable<Array<ProductCosif>>;

  constructor(
    private manualMovementApiService: ManualMovementApiService,
    private manualMovementStoreService: ManualMovementStoreService,
    private productApiService: ProductApiService,
    private productStoreService: ProductStoreService) {

    this.isManualMovementsLoading$ = manualMovementStoreService.isManualMovementsLoading$;
    this.manualMovements$ = manualMovementStoreService.manualMovements$;
    this.isProductsLoading$ = productStoreService.isProductsLoading$;
    this.products$ = productStoreService.products$;
    this.isProductCosifsLoading$ = productStoreService.isProductCosifsLoading$;
    this.productCosifs$ = productStoreService.productCosifs$;
  }

  getManualMovements(): void {
    this.manualMovementStoreService.isManualMovementsLoading = true;
    this.manualMovementStoreService.manualMovements = null;

    this.manualMovementApiService.getManualMovements()
      .subscribe((manualMovements: Array<ManualMovement>) => {
        this.manualMovementStoreService.isManualMovementsLoading = false;
        this.manualMovementStoreService.manualMovements = manualMovements;
      });
  }

  getProducts(): void {
    this.productStoreService.isProductsLoading = true;
    this.productStoreService.products = null;

    this.productApiService.getProducts()
      .subscribe((products: Array<Product>) => {
        this.productStoreService.isProductsLoading = false;
        this.productStoreService.products = products;
      });
  }

  getProductCosifs(codProduct: string): void {
    this.productStoreService.isProductCosifsLoading = true;
    this.productStoreService.productCosifs = null;

    this.productApiService.getProductCosifs(codProduct)
      .subscribe((productCosifs: Array<ProductCosif>) => {
        this.productStoreService.isProductCosifsLoading = false;
        this.productStoreService.productCosifs = productCosifs;
      });
  }

  createManualMovement(manualMovementToCreate: object): Observable<ManualMovement> {
    return this.manualMovementApiService.createManualMovement(manualMovementToCreate);
  }
}
