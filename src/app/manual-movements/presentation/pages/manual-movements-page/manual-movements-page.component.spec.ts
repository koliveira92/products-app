import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualMovementsPageComponent } from './manual-movements-page.component';
import { ManualMovementFacadeService } from '../../facades/manual-movements-facade.service';

describe('MovementsPageComponent', () => {
  let component: ManualMovementsPageComponent;
  let fixture: ComponentFixture<ManualMovementsPageComponent>;
  let manualMovementFacadeService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule
      ],
      providers: [],
      declarations: [ManualMovementsPageComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualMovementsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    manualMovementFacadeService = TestBed.inject(ManualMovementFacadeService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get product cosifs', () => {
    const getProductCosifs = spyOn(manualMovementFacadeService, 'getProductCosifs').and.callThrough();

    component.handleProductChange('0001');

    expect(getProductCosifs).toHaveBeenCalledWith('0001');
  });

  it('should create and get manual movements', () => {
    const createManualMovement = spyOn(manualMovementFacadeService, 'createManualMovement').and.callThrough();

    component.handleProductCreate({});

    expect(createManualMovement).toHaveBeenCalledWith({});
  });
});
