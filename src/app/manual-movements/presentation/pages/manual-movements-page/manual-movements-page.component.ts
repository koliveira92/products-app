import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { ManualMovement } from '../../../models/manual-movements.model';
import { Product } from '../../../models/product.model';
import { ProductCosif } from '../../../models/product-cosif.model';
import { ManualMovementFacadeService } from '../../facades/manual-movements-facade.service';

@Component({
  selector: 'app-manual-movements-page',
  templateUrl: './manual-movements-page.component.html',
  styleUrls: ['./manual-movements-page.component.css']
})
export class ManualMovementsPageComponent implements OnInit {

  isManualMovementsLoading$: Observable<boolean>;

  manualMovements$: Observable<Array<ManualMovement>>;

  isProductsLoading$: Observable<boolean>;

  products$: Observable<Array<Product>>;

  isProductCosifsLoading$: Observable<boolean>;

  productCosifs$: Observable<Array<ProductCosif>>;

  constructor(
    private manualMovementFacadeService: ManualMovementFacadeService) {

    this.isManualMovementsLoading$ = this.manualMovementFacadeService.isManualMovementsLoading$;
    this.manualMovements$ = this.manualMovementFacadeService.manualMovements$;
    this.isProductsLoading$ = this.manualMovementFacadeService.isProductsLoading$;
    this.products$ = this.manualMovementFacadeService.products$;
    this.isProductCosifsLoading$ = this.manualMovementFacadeService.isProductCosifsLoading$;
    this.productCosifs$ = this.manualMovementFacadeService.productCosifs$;
  }

  ngOnInit(): void {
    this.manualMovementFacadeService.getManualMovements();
    this.manualMovementFacadeService.getProducts();
  }

  handleProductChange(codProduct: string): void {
    this.manualMovementFacadeService.getProductCosifs(codProduct);
  }

  handleProductCreate(manualMovementToCreate: object): void {
    this.manualMovementFacadeService.createManualMovement(manualMovementToCreate)
      .subscribe(() => {
        this.manualMovementFacadeService.getManualMovements();
      });
  }
}
