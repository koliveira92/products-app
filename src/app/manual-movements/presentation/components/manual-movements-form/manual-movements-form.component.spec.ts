import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

import { ManualMovementsFormComponent } from './manual-movements-form.component';

describe('ManualMovementsFormComponent', () => {
  let component: ManualMovementsFormComponent;
  let fixture: ComponentFixture<ManualMovementsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule
      ],
      providers: [],
      declarations: [ManualMovementsFormComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualMovementsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit productChange', () => {
    const productChangeEmit = spyOn(component.productChange, 'emit').and.callThrough();

    component.handleProductChange({ value: '0001' });

    expect(productChangeEmit).toHaveBeenCalledWith('0001');
  });

  it('should emit enable from', () => {
    const manualMovementFormEnable = spyOn(component.manualMovementForm, 'enable').and.callThrough();

    component.handleProductNew();

    expect(manualMovementFormEnable).toHaveBeenCalledWith();
  });

  it('should emit disable from', () => {
    const manualMovementFormEnable = spyOn(component.manualMovementForm, 'disable').and.callThrough();

    component.handleProductClear();

    expect(manualMovementFormEnable).toHaveBeenCalledWith();
  });

  it('should emit productCreate', () => {
    const productCreateEmit = spyOn(component.productCreate, 'emit').and.callThrough();

    component.handleProductCreate();

    expect(productCreateEmit).toHaveBeenCalledWith(component.manualMovementForm.value);
  });
});
