import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { ManualMovement } from '../../../models/manual-movements.model';
import { Product } from '../../../models/product.model';
import { ProductCosif } from '../../../models/product-cosif.model';
import { typeWithParameters } from '@angular/compiler/src/render3/util';

@Component({
  selector: 'app-manual-movements-form',
  templateUrl: './manual-movements-form.component.html',
  styleUrls: ['./manual-movements-form.component.css']
})
export class ManualMovementsFormComponent implements OnInit {

  @Input() isProductsLoading: boolean;

  @Input() products: Array<Product>;

  @Input() isProductCosifsLoading: boolean;

  @Input() productCosifs: Array<ProductCosif>;

  @Output() productChange = new EventEmitter<string>();

  @Output() productCreate = new EventEmitter<void>();

  @ViewChild('iptMonth') iptMonth: ElementRef;

  manualMovementForm = this.formBuilder.group({
    DatMonth: ['', Validators.required],
    DatYear: ['', Validators.required],
    CodProduct: ['', Validators.required],
    CodCosif: ['', Validators.required],
    ValValue: ['', Validators.required],
    DesDescription: ['', Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.disableForm();
  }

  handleProductChange(event): void {
    this.productChange.emit(event.value);
  }

  handleProductNew(): void {
    this.enableForm();
  }

  handleProductClear(): void {
    this.disableForm();
  }

  handleProductCreate(): void {
    this.productCreate.emit(this.manualMovementForm.value);
    this.disableForm();
  }

  private enableForm(): void {
    this.manualMovementForm.enable();

    if (this.iptMonth) {
      this.iptMonth.nativeElement.focus();
    }
  }

  private disableForm(): void {
    this.manualMovementForm.disable();
    this.manualMovementForm.reset();
  }
}
