import { Component, Input } from '@angular/core';

import { ManualMovement } from '../../../models/manual-movements.model';

@Component({
  selector: 'app-manual-movements-list',
  templateUrl: './manual-movements-list.component.html',
  styleUrls: ['./manual-movements-list.component.css']
})
export class ManualMovementsListComponent {

  @Input() isManualMovementsLoading: boolean;

  @Input() manualMovements: Array<ManualMovement>;

  DISPLAYED_COLUMNS: string[] = ['month', 'year', 'codProduct', 'desProduct', 'numRelease', 'description', 'value'];
}
