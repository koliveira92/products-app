import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Product } from '../models/product.model';
import { ProductCosif } from '../models/product-cosif.model';
import { BaseApiService } from '../../shared/services/base-api.service';

@Injectable({
  providedIn: 'root'
})
export class ProductApiService extends BaseApiService {

  private BASE_PATH = 'api/products/';

  getProducts(): Observable<Array<Product>> {
    return this.http.get<Array<Product>>(`${this.BASE_API_PATH}${this.BASE_PATH}`);
  }

  getProductCosifs(codProduct: string): Observable<Array<ProductCosif>> {
    return this.http.get<Array<ProductCosif>>(`${this.BASE_API_PATH}${this.BASE_PATH}${codProduct}/cosifs`);
  }
}
