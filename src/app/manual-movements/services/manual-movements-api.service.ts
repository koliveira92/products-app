import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ManualMovement } from '../models/manual-movements.model';
import { BaseApiService } from '../../shared/services/base-api.service';

@Injectable({
  providedIn: 'root'
})
export class ManualMovementApiService extends BaseApiService {

  private BASE_PATH = 'api/manualmovements/';

  getManualMovements(): Observable<Array<ManualMovement>> {
    return this.http.get<Array<ManualMovement>>(`${this.BASE_API_PATH}${this.BASE_PATH}`);
  }

  createManualMovement(manualMovementToCreate: object): Observable<ManualMovement> {
    return this.http.post<ManualMovement>(`${this.BASE_API_PATH}${this.BASE_PATH}`, manualMovementToCreate);
  }
}
