export interface Product {
  CodProduct: string;
  DesProduct: string;
  StaStatus: number;
}
