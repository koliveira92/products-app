import { ProductCosif } from './product-cosif.model';

export interface ManualMovement {
  DatMonth: number;
  DatYear: number;
  NumRelease: number;
  CodProduct: string;
  CodCosif: string;
  DesDescription: string;
  DatMovement: Date;
  CodUser: string;
  ValValue: number;
  ProductCosif: ProductCosif;
}
