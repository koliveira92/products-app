import { Product } from './product.model';

export interface ProductCosif {
  CodProduct: string;
  CodCosif: string;
  CodClassification: string;
  StaStatus: number;
  Product: Product;
}
