import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseApiService {

  protected BASE_API_PATH = environment.BASE_API_PATH;

  constructor(protected http: HttpClient) { }
}
